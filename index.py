import zipfile
import requests
import io
import os
import pandas as pd
from matplotlib import pyplot as plt

if not os.path.exists('2020_AR_Region_Mobility_Report.csv') or not os.path.exists('2021_AR_Region_Mobility_Report.csv'):
    r = requests.get('https://www.gstatic.com/covid19/mobility/Region_Mobility_Report_CSVs.zip', stream=True)
    z = zipfile.ZipFile(io.BytesIO(r.content))
    z.extract('2020_AR_Region_Mobility_Report.csv')
    z.extract('2021_AR_Region_Mobility_Report.csv')

df = pd.concat([
    pd.read_csv('2020_AR_Region_Mobility_Report.csv', parse_dates=['date']), 
    pd.read_csv('2021_AR_Region_Mobility_Report.csv', parse_dates=['date'])])
df = df.where(df.date >= '2021-01-01').groupby('date').mean().rolling(14).mean()

plt.title('Variación porcentual de la movilidad a estaciones de tránsito en CABA')
plt.figtext(.5,.0, 'Fuente: Reportes de Movilidad de Google. Código fuente: https://gitlab.com/nmercado1986/movilidad-caba', fontsize=11, ha='center', va='bottom')
plt.plot(df.index, df['transit_stations_percent_change_from_baseline'])
plt.show()


