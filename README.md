## Gráfico de datos de movilidad a estaciones de tránsito en CABA

Sentite libre de hacer modificaciones / forks.
El archivo de los datos de movilidad de Google se filtra a Buenos Aires (excluyendo Buenos Aires Province).
Hay otras columnas que se pueden usar.

Para ejecutar:

```
pipenv install
pipenv shell
python index.py
```
